from django.db import models

# Create your models here.

class Social(models.Model):
    name = models.CharField(max_length=100)
    icon = models.CharField(max_length=130)
    link = models.URLField()

    credit_date = models.DateTimeField(auto_now=True)
    modified_date = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.name